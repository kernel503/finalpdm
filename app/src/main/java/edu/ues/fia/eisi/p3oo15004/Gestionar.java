package edu.ues.fia.eisi.p3oo15004;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Gestionar extends AppCompatActivity {

    private static final String Tag = "Mensajes";
    EditText edt1, edt2, edt3, edt4, edt5;
    ControlBD BDhelper;
    private String urlSentencia = "http://192.168.1.8/oo_sentencia.php?q=";
    private String urlConsulta = "http://192.168.1.8/oo_consulta.php?q=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestionar);

        BDhelper=new ControlBD(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder() .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        edt1 = findViewById(R.id.editText);
        edt2 = findViewById(R.id.editText2);
        edt3 = findViewById(R.id.editText3);
        edt4 = findViewById(R.id.editText4);
        edt5 = findViewById(R.id.editText5);

    }

    public void bt1(View v){
        String query = String.format("CALL insertar('%s','%s','%s')",edt1.getText().toString(),edt2.getText().toString(),edt3.getText().toString());
        query= query.replaceAll("\u0025" ,"%25").replaceAll("\u003D" ,"%3D").replaceAll("\u003C" ,"%3C").replaceAll("\u003E" ,"%3E").replaceAll(" ", "%20").replaceAll("'", "%27").replaceAll("\\(", "%28").replaceAll("\\)", "%29").replaceAll("\\*","%2A");
        Log.i(Tag, urlSentencia+query);
        Toast.makeText(this,ControladorServicio.sentencia(urlSentencia+query),Toast.LENGTH_SHORT).show();
    }
    public void bt2(View v){
        String query = String.format("CALL actualizar('%s','%s','%s')",edt1.getText().toString(),edt2.getText().toString(),edt3.getText().toString());
        query= query.replaceAll("\u0025" ,"%25").replaceAll("\u003D" ,"%3D").replaceAll("\u003C" ,"%3C").replaceAll("\u003E" ,"%3E").replaceAll(" ", "%20").replaceAll("'", "%27").replaceAll("\\(", "%28").replaceAll("\\)", "%29").replaceAll("\\*","%2A");
        Log.i(Tag, urlSentencia+query);
        Toast.makeText(this,ControladorServicio.sentencia(urlSentencia+query),Toast.LENGTH_SHORT).show();
    }
    public void bt3(View v){
        String query = String.format("CALL eliminar('%s')",edt1.getText().toString());
        query= query.replaceAll("\u0025" ,"%25").replaceAll("\u003D" ,"%3D").replaceAll("\u003C" ,"%3C").replaceAll("\u003E" ,"%3E").replaceAll(" ", "%20").replaceAll("'", "%27").replaceAll("\\(", "%28").replaceAll("\\)", "%29").replaceAll("\\*","%2A");
        Log.i(Tag, urlSentencia+query);
        Toast.makeText(this,ControladorServicio.sentencia(urlSentencia+query),Toast.LENGTH_SHORT).show();
    }
    public void bt4(View v){
        edt2.setText("");
        edt3.setText("");
        String query = String.format("CALL consultar('%s')",edt1.getText().toString());
        query= query.replaceAll("\u0025" ,"%25").replaceAll("\u003D" ,"%3D").replaceAll("\u003C" ,"%3C").replaceAll("\u003E" ,"%3E").replaceAll(" ", "%20").replaceAll("'", "%27").replaceAll("\\(", "%28").replaceAll("\\)", "%29").replaceAll("\\*","%2A");
        Log.i(Tag, urlSentencia+query);
        //Toast.makeText(this,ControladorServicio.consulta(urlConsulta+query),Toast.LENGTH_SHORT).show();
        String valor = ControladorServicio.consulta(urlConsulta+query);
        if (!valor.equals("No encontrado")){
            String [] partes = valor.split("-");
            edt2.setText(partes[0]);
            edt3.setText(partes[1]);
        }else{
            edt2.setText(valor);
        }

    }

}

