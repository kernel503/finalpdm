package edu.ues.fia.eisi.p3oo15004;

public class Respaldo {
    String id, campo1, campo2, campo3;
    public Respaldo() {
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getCampo1() {
        return campo1;
    }
    public void setCampo1(String campo1) {
        this.campo1 = campo1;
    }
    public String getCampo2() {
        return campo2;
    }
    public void setCampo2(String campo2) {
        this.campo2 = campo2;
    }
    public String getCampo3() {
        return campo3;
    }
    public void setCampo3(String campo3) {
        this.campo3 = campo3;
    }
}
