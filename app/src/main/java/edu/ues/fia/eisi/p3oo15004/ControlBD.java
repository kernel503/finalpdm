package edu.ues.fia.eisi.p3oo15004;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;

public class ControlBD {

    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;
    private static final String Tag = "Mensajes";

    public ControlBD(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        private static final String BASE_DATOS = "oo15004.s3db";
        private static final int VERSION = 1;

        public DatabaseHelper(Context context) {
            super(context, BASE_DATOS, null, VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try{

                db.execSQL("CREATE TABLE depto2(coddepto VARCHAR(5) NOT NULL PRIMARY KEY," +
                        "nomdepto VARCHAR(30),zona VARCHAR(30));");
                //db.execSQL("CREATE TABLE materia(codmateria VARCHAR(6) NOT NULL PRIMARY KEY," +
                //        "nommateria VARCHAR(30),unidadesval VARCHAR(1));");
                //db.execSQL("CREATE TABLE nota(carnet VARCHAR(7) NOT NULL ,codmateria VARCHAR(6)" +
                //       " NOT NULL ,ciclo VARCHAR(5) ,notafinal REAL ," +
                //       "PRIMARY KEY(carnet,codmateria,ciclo));");
                Log.i(Tag, "TABLA CREADA");
            }catch(SQLException e){
                e.printStackTrace();
                Log.i(Tag, "NO SE CREO LA TABLA");
            }
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub
        }

    }

    public void abrir() throws SQLException{
        db = DBHelper.getWritableDatabase();
        return;
    }

    public void cerrar(){
        DBHelper.close();
    }

    public String ingresarSQLITE(ArrayList lista){
        String s="";
        abrir();
        ArrayList<Apoyo> m = lista;
        //ESTE MODIFICAR SEGUN CANTIDAD DE ARGUMENTOS
        String[] args = new String[3];
        int regAfectados = 0;
        for (Apoyo a:m){
            args[0]=a.getId();
            args[1]=a.getCampo1();
            args[2]=a.getCampo2();
            try{
                db.execSQL("INSERT OR IGNORE INTO depto2 VALUES (?,?,?);",args);
                args[0]=a.getCampo1();
                args[1]=a.getCampo2();
                args[2]=a.getId();
                db.execSQL("UPDATE depto2 SET nomdepto = ?, zona = ? WHERE coddepto = ?",args);
                regAfectados++;
            }catch (SQLException ex){
                Log.i(Tag, "ERROR EN EL INSERT");
            }
        }
        if (regAfectados == 0){
            s = "Existen registros repetidos";
        }else{
            s = "Registros afectados: "+ regAfectados;
        }
        cerrar();
        return  s;
    }

    public String actualizarSQLITE(ArrayList lista){
        String s="";
        abrir();
        ArrayList<Apoyo> m = lista;
        //ESTE MODIFICAR SEGUN CANTIDAD DE ARGUMENTOS
        String[] args = new String[3];
        int regAfectados = 0;
        for (Apoyo a:m){
            args[0]=a.getCampo1();
            args[1]=a.getCampo2();
            args[2]=a.getId();
            try{
                db.execSQL("UPDATE depto2 SET nomdepto = ?, zona = ? WHERE coddepto = ?",args);
                regAfectados++;
                s = "Registros Actulizados";
            }catch (SQLException ex){
                Log.i(Tag, "ERROR EN EL UPDATE");
                s = "No se pudo Actualizar";
            }
        }
        cerrar();
        return  s;
    }

    public ArrayList listaSQLITE(){
        abrir();
        ArrayList<Apoyo> lista = new ArrayList<>();
        try{
            Cursor c= db.rawQuery("SELECT * FROM depto2",null);
            if(c.moveToFirst()){
                do{
                    Apoyo u = new Apoyo();
                    u.setId(c.getString(0));
                    u.setCampo1(c.getString(1));
                    u.setCampo2(c.getString(2));;
                    lista.add(u);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){
            Log.w(Tag, "ERROR EN LISTAR DATOS SQLITE");
        }
        cerrar();
        return lista;
    }

}
