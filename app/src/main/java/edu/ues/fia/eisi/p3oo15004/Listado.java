package edu.ues.fia.eisi.p3oo15004;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class Listado extends AppCompatActivity {

    private String urlSentencia = "http://192.168.1.8/oo_sentencia.php?q=";
    private String urlConsulta = "http://192.168.1.8/oo_consulta.php?q=";
    private static final String Tag = "Mensajes";

    EditText edit1,edit2;
    ListView listView;
    ControlBD BDhelper;

    ArrayList<Apoyo> lista;
    private ArrayList<String> stringArrayList;
    private ArrayAdapter<String> stringArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado);

        BDhelper=new ControlBD(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder() .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        edit1 = findViewById(R.id.editLista);
        edit2 = findViewById(R.id.edit2Lista);
        listView = findViewById(R.id.listview1);
    }

    public void consultarLista1(View v){
        //String query = String.format("SELECT * FROM depto WHERE zona like '%s'",edit1.getText().toString());
        String query = "CALL lista('"+edit1.getText().toString()+"')";
        query= query.replaceAll("\u0025" ,"%25").replaceAll("\u003D" ,"%3D").replaceAll("\u003C" ,"%3C").replaceAll("\u003E" ,"%3E").replaceAll(" ", "%20").replaceAll("'", "%27").replaceAll("\\(", "%28").replaceAll("\\)", "%29").replaceAll("\\*","%2A");
        Log.i(Tag, urlConsulta+query);
        lista = ControladorServicio.obtenerLista(urlConsulta+query);
        llenarTabla();
    }

    public void guardarLista1(View v){
        Toast.makeText(this,BDhelper.ingresarSQLITE(lista),Toast.LENGTH_SHORT).show();
        //Toast.makeText(this,BDhelper.actualizarSQLITE(lista),Toast.LENGTH_SHORT).show();
        lista.clear();
        llenarTabla();
    }

    private void llenarTabla(){
        stringArrayList= new ArrayList<>();
        if (!lista.isEmpty()){
            for (Apoyo a : lista){
                stringArrayList.add(a.getId()+"\n"+a.getCampo1()+"\n"+a.getCampo2());
            }
        }else {
            stringArrayList.add("Lista Vacia");
        }
        stringArrayAdapter =new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1,stringArrayList);
        listView.setAdapter(stringArrayAdapter);
    }

}
