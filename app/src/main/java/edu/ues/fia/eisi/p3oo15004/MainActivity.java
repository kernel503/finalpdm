package edu.ues.fia.eisi.p3oo15004;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String Tag = "Mensajes";
    ControlBD BDhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BDhelper=new ControlBD(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder() .permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public void btn1M(View V){
        //SEGUNDO DEBE SER ACTIVITY DESTINO
        Intent myIntent = new Intent(this, Gestionar.class);
        startActivity(myIntent);
    }
    public void btn2M(View V){
        Intent myIntent = new Intent(this, Listado.class);
        startActivity(myIntent);
    }
    public void btn3M(View V){
        Intent myIntent = new Intent(this, MainActivity.class);
        startActivity(myIntent);
    }
    public void btn4M(View V){
        //Intent myIntent = new Intent(this, MainActivity.class);
        //startActivity(myIntent);
        String urlSentencia = "http://192.168.1.8/oo_sentencia.php?q=";

        ArrayList<Apoyo> lista = new ArrayList<>();
        lista=BDhelper.listaSQLITE();

        for (Apoyo m : lista){
            String query = String.format("CALL insertar('%s','%s','%s')",m.getId(),m.getCampo1(),m.getCampo2());
            query= query.replaceAll("\u0025" ,"%25").replaceAll("\u003D" ,"%3D").replaceAll("\u003C" ,"%3C").replaceAll("\u003E" ,"%3E").replaceAll(" ", "%20").replaceAll("'", "%27").replaceAll("\\(", "%28").replaceAll("\\)", "%29").replaceAll("\\*","%2A");
            Log.i(Tag, urlSentencia+query);
            ControladorServicio.sentencia(urlSentencia+query);

            String query2 = String.format("CALL actualizar('%s','%s','%s')",m.getId(),m.getCampo1(),m.getCampo2());
            query2= query2.replaceAll("\u0025" ,"%25").replaceAll("\u003D" ,"%3D").replaceAll("\u003C" ,"%3C").replaceAll("\u003E" ,"%3E").replaceAll(" ", "%20").replaceAll("'", "%27").replaceAll("\\(", "%28").replaceAll("\\)", "%29").replaceAll("\\*","%2A");
            Log.i(Tag, urlSentencia+query2);
            ControladorServicio.sentencia(urlSentencia+query2);

        }



    }

}
