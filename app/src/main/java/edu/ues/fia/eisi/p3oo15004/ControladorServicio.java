package edu.ues.fia.eisi.p3oo15004;

import android.util.Log;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class ControladorServicio {

    private static final String Tag = "Mensajes";

    public static String obtenerRespuestaPeticion(String url) {
        String respuesta = " ";
        HttpParams parametros = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(parametros, 3000);
        HttpConnectionParams.setSoTimeout(parametros, 5000);
        HttpClient cliente = new DefaultHttpClient(parametros);
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse httpRespuesta = cliente.execute(httpGet);
            StatusLine estado = httpRespuesta.getStatusLine();
            int codigoEstado = estado.getStatusCode();
            if (codigoEstado == 200) {
                HttpEntity entidad = httpRespuesta.getEntity();
                respuesta = EntityUtils.toString(entidad);
            }
        } catch (Exception e) {
            Log.i(Tag, "Error en la conexion (Obtener Respuesta Peticion)");
        }
        return respuesta;
    }

    public static String sentencia(String peticion) {
        //Log.i(Tag, "URL: "+peticion);
        String json = obtenerRespuestaPeticion(peticion);
        Log.i(Tag, "JSON: "+json);
        String respuesta;
        try {
            JSONObject resultado = new JSONObject(json);
            respuesta = resultado.getString("resultado");
            //Log.i(Tag, "Respuesta PHP: "+respuesta);
        }
        catch (JSONException e) {
            e.printStackTrace();
            respuesta = "Error en parseOO de JSON";
            //Log.i(Tag, "ERROR PARSEO");
        }
        Log.i(Tag, "**********************************************************");
        return respuesta;
    }

    public static String consulta(String peticion) {
        //Log.i(Tag, "URL: "+peticion);
        String json = obtenerRespuestaPeticion(peticion);
        Log.i(Tag, "JSON: "+json);
        String valor="";
        try {
            JSONArray JSON = new JSONArray(json);
            if (JSON.length()==0){
                valor = "No encontrado";
            }
            for (int i = 0; i < JSON.length(); i++) {
                JSONObject obj = JSON.getJSONObject(i);
                valor = obj.getString("valor");
            }
        } catch (Exception e) {
            Log.w(Tag, "ERROR OBTENER VALOR DE FUNCION");
            valor = "No encontrado";
        }
        Log.i(Tag, "**********************************************************");
        return valor;
    }

    public static ArrayList obtenerLista(String peticion) {
        //Log.i(Tag, "URL: "+peticion);
        String json = obtenerRespuestaPeticion(peticion);
        Log.i(Tag, "JSON: "+json);
        ArrayList<Apoyo> lista = new ArrayList<>();
        try {
            JSONArray JSON = new JSONArray(json);
            for (int i = 0; i < JSON.length(); i++) {
                JSONObject obj = JSON.getJSONObject(i);
                Apoyo a = new Apoyo();
                a.setId(obj.getString("coddepto"));
                a.setCampo1(obj.getString("nomdepto"));
                a.setCampo2(obj.getString("zona"));
                lista.add(a);
            }
        } catch (Exception e) {
            Log.w(Tag, "ERROR OBTENER ARRAY");
        }
        Log.i(Tag, "**********************************************************");
        return lista;
    }

}
